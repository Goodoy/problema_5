//Convertir un int en un string, posicion por posicion del int y agregar uno a uno en un string.*/

#include <iostream>

int longitudInt(int n);  //Recibe un int y verifica la longitud.
void intToC(int *numero,char *list);  //funcion que cambia el int a char recibiendo el int y un arreglo(String).
void limpiarC(char *lista,int tam);  //Limpia o inicializa el string.

using namespace std;

int main()
{
    int numero,lon; //Inicializa los int

    cout<<"Numero : "; cin>>numero; // Recibe el int

    lon=longitudInt(numero); //Busca la longitud del int

    char numero2[lon]; //Inicializa el char en el que se va a almacenal el int

    limpiarC(numero2,lon); //Se inicializa el char

    intToC(&numero,numero2);  // Convierte el int y lo guarda en numero2

    cout<<numero2; //imprime el string

    return 0;
}

int longitudInt(int n)  //Recibe un int y busca su longitud.
{
    int longi=0; //incia la variable
    while(n>0){  //Hasta que n sea menor que 0
        n=n/10;  //saca el digito
        longi++; //cuenta la iteracion que es la longitud del int
    }
    return longi;
}

void intToC(int *numero,char *list){ //
    int lon,aux,digito,auxlon; //inicia las variables

    aux=*numero; //aux guarda la posicion del puntero numero
    lon=longitudInt(*numero); //Toma la longitud del numero
    auxlon=lon-1; //aux de long

    while(aux>0){ //Recorre hasta que termine con int
        digito=aux%10; //guarda el digito
        aux=aux/10; //Lo elimina
        *(list+auxlon)=char(48+digito); //Lo guarda en la posicion list+auxlon convertido a string
        auxlon--;                       //va recorriendolo de atrás hacia adelante
    }
}

void limpiarC(char *lista,int tam){ //Recibe un arreglo y su tamaño
    int i;
    for(i=0;i<tam; i++){ //Recorre todo un arreglo
        *(lista+i)=' ';  //Entra e la posicion lista+i y le pone ' '
    }
}
